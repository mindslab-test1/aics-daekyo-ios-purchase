# [P471] 대교 상상키즈 iOS 결제 관리 서버

해당 서버는 대교 상상키즈 iOS 인앱 결제 시에 결제 완료 및 정기 과금 완료 시의 동작을 관리하기 위한 서버입니다.

Developer : Byeonguk.yu (byeonguk.yu@mindlsab.ai)



### Used Language & Framework

- Java 1.8
- Spring boot 2.3.4.RELEASE
- MyBatis 2.1.3
- Maven



### Setting Project

- #### Setup SSL Key file

  - Apple 에서 서버에 Request 요청 시, https 로 보안 처리가 되어있지 않은 서버는 정상적인 연결이 이루어지지 않으므로, SSL 인증 처리가 반드시 필요합니다.
    [관련링크](https://developer.apple.com/documentation/security/preventing_insecure_network_connections)

  - 사용 할 도메인의 SSL 인증서 파일을 준비하여 셋팅합니다.

  - Spring boot 특성 상, 인증서 본문 파일 + 인증서 키 파일 + CA사 인증 키 파일 을 번들링한 .jks 파일이 필요합니다.

  - Dev 파라미터로 실행 할 경우 maum.ai 도메인 인증서가,

  - Prod 파라미터로 실행 할 경우 sangsangkids.co.kr 도메인 인증서를 불러오도록 사전 설정 되어있으며, 
    해당 jks 파일은 git Repo에 **포함 되어 있지 않습니다.** 
    필요한 경우 AICS 개발팀으로 문의 주세요.

    

- #### Insert Argument

  - Source Code 내에 Application.yml 을 확인 해 보면 dev / local / prod 3가지 인자에 대한 사전 설정 파일을 준비 해 두었습니다.

  - 해당 사전 설정 외에 별도 커스텀이 필요하신 경우 별도 Branch 와 yml 파일을 생성하여 PR 요청 주시기 바랍니다. 
    (사전 협의 되지 않은 커스텀 작업에 한하여 지원이 어렵다는 점 미리 알립니다.)

    

- #### Build & Run

  - 이후 Build 과정은 일반적인 Spring Boot Project와 동일합니다.
  - Maven Build (jar) & Run Server



### To Do

- Code refactoring
  - 단기간 내에 급하게 만든 코드라 재사용성에 대한 부분이 미비함
- 여 타 iOS 서비스 들과 상호작용
  - Server 최초 구상 시에, 결제 관리 서버 - 각 서비스별 별도 서버 로
    두 End-point 를 염두 해 두고 작업을 어느정도 진행 함.
  - 관련해서 서비스 로직을 대교 전용 로직 / iOS 결제 관리 로직 을 분리 후
    추후 Architecture 를 분리 할 수도 있을 예정



P.S) 이정도면 정말 빡세게 쓴 듯 함.

P.S2) 그 외 질문 사항은 @CuroGom 으로 Slack DM / byeonguk.yu@mindslab.ai 로 메일 문의 주세요.