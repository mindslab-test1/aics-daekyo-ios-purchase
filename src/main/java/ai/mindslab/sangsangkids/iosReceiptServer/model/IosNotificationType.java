package ai.mindslab.sangsangkids.iosReceiptServer.model;

public enum IosNotificationType {
    CANCEL("CANCEL"),
    DID_CHANGE_RENEWAL_PREF("DID_CHANGE_RENEWAL_PREF"),
    DID_CHANGE_RENEWAL_STATUS("DID_CHANGE_RENEWAL_STATUS"),
    DID_FAIL_TO_RENEW("DID_FAIL_TO_RENEW"),
    DID_RECOVER("DID_RECOVER"),
    DID_RENEW("DID_RENEW"),
    INITIAL_BUY("INITIAL_BUY"),
    INTERACTIVE_RENEWAL("INTERACTIVE_RENEWAL"),
    PRICE_INCREASE_CONSENT("PRICE_INCREASE_CONSENT"),
    REFUND("REFUND"),
    RENEWAL("CANCRENEWALEL");


    private String typeString;
    private boolean isGoing;

    IosNotificationType(String typeString) {
        this.typeString = typeString;
    }

}
