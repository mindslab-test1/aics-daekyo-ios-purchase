package ai.mindslab.sangsangkids.iosReceiptServer.model;

import lombok.Data;
import lombok.ToString;

import java.util.List;
import java.util.Map;

@Data
@ToString
public class IosUnifiedReceipt {
    private String latestReceipt;
    private List<Map> pendingRenewalInfo;
    private String environment;
    private int status;
    private List<IosNotiReceiptInfo> latestReceiptInfo;
}
