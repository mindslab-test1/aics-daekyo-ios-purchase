package ai.mindslab.sangsangkids.iosReceiptServer.model;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class IosNotificationResponse {
    private IosUnifiedReceipt unifiedReceipt;
    private String environment;
    private String autoRenewStatus;
    private String bvrs;
    private String autoRenewProductId;
    private String notificationType;
    private int expirationIntent;
}
