package ai.mindslab.sangsangkids.iosReceiptServer.model;

import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
public class IosReceipt {
    private String receiptType;
    private int adamId;
    private int appItemId;
    private String bundleId;
    private String downloadId;
    private String receiptCreationDateMs;
    private String requestDateMs;
    private List<IosNotiReceiptInfo> inApp;
}
