package ai.mindslab.sangsangkids.iosReceiptServer.model;

import lombok.Data;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Data
@ToString
public class IosReceiptVerityTemp {
    private String environment;
    private int status;
    private IosReceipt receipt;
    private IosNotiReceiptInfo latestReceiptInfo;
    private String latestReceipt;

    public IosReceiptVerityResponse convertResponse() {
        IosReceiptVerityResponse result = new IosReceiptVerityResponse();
        List<IosNotiReceiptInfo> iosNotiReceiptInfoList = new ArrayList<>();
        iosNotiReceiptInfoList.add(this.latestReceiptInfo);

        result.setEnvironment(this.environment);
        result.setStatus(this.status);
        result.setReceipt(receipt);
        result.setLatestReceiptInfo(iosNotiReceiptInfoList);
        result.setLatestReceipt(this.latestReceipt);

        return result;
    }
}
