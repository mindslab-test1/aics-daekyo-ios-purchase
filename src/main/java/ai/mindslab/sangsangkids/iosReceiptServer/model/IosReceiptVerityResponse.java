package ai.mindslab.sangsangkids.iosReceiptServer.model;

import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
public class IosReceiptVerityResponse {
    private String environment;
    private int status;
    private IosReceipt receipt;
    private List<IosNotiReceiptInfo> latestReceiptInfo;
    private String latestReceipt;
}
