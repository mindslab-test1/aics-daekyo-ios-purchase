package ai.mindslab.sangsangkids.iosReceiptServer.model;

import lombok.Data;

@Data
public class DKAlreadyPayedParams {
    private int userId;
}
