package ai.mindslab.sangsangkids.iosReceiptServer.model;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class IosReceiptVerifyParams {
    private String receiptData;
    private int userId;
}
