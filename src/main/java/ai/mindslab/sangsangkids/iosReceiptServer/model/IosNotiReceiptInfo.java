package ai.mindslab.sangsangkids.iosReceiptServer.model;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class IosNotiReceiptInfo {
    private int userId;
    private int ishId;
    private String expiresDatePst;
    private String purchaseDate;
    private String purchaseDateMs;
    private String originalPurchaseDateMs;
    private String transactionId;
    private String originalTransactionId;
    private String quantity;
    private String expiresDateMs;
    private String originalPurchaseDatePst;
    private String productId;
    private String subscriptionGroupIdentifier;
    private String webOrderLineItemId;
    private String expiresDate;
    private String isInIntroOfferPeriod;
    private String originalPurchaseDate;
    private String purchaseDatePst;
    private String isTrialPeriod;
    private String cancellationDateMs;
}