package ai.mindslab.sangsangkids.iosReceiptServer.controller;

import ai.mindslab.sangsangkids.iosReceiptServer.exception.IosReceiptException;
import ai.mindslab.sangsangkids.iosReceiptServer.model.DKAlreadyPayedParams;
import ai.mindslab.sangsangkids.iosReceiptServer.model.IosReceiptVerifyParams;
import ai.mindslab.sangsangkids.iosReceiptServer.service.IosReceiptService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class IosReceiptController {
    private final IosReceiptService service;
    private static final Logger logger = LoggerFactory.getLogger(IosReceiptController.class);

    public IosReceiptController(IosReceiptService service) {
        this.service = service;
    }

    @RequestMapping("/alreadyPaid")
    public ResponseEntity alreadyPayed (@RequestBody DKAlreadyPayedParams params) {
        logger.info("Request AlreadyPaid");
        logger.info(params.toString());

        boolean result;
        try {
            result = service.alreadyPaid(params);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        //TODO : 임시조치 코드
        result = false;

        return ResponseEntity.ok(result);
    }

    @RequestMapping("/verifyReceipt")
    public ResponseEntity verifyReceipt (@RequestBody IosReceiptVerifyParams params) {
        logger.info("Requested verifyReceipt");
        int result;
        try {
            result = service.verifyReceipt(params);
        } catch (Exception e) {
           logger.error(e.getMessage(), e);
           return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(result);
    }

    @RequestMapping("/subscribeNoti")
    public ResponseEntity subscribeNoti(HttpServletRequest request) {
        try {
            service.subscribeNoti(request);
        } catch (IosReceiptException exception) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok().build();
    }

    @RequestMapping("/completePurchase")
    public ResponseEntity completePurchase(@RequestBody IosReceiptVerifyParams params) {
        try {
            service.completePurchase(params);
        } catch (IosReceiptException exception) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok().build();
    }
}
