package ai.mindslab.sangsangkids.iosReceiptServer;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(basePackages = "ai.mindslab.sangsangkids.iosReceiptServer.repository.mapper")
public class SangsangkidsIosReceiptApplication {

    public static void main(String[] args) {
        SpringApplication.run(SangsangkidsIosReceiptApplication.class, args);
    }

}
