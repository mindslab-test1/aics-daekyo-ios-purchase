package ai.mindslab.sangsangkids.iosReceiptServer.service;

import ai.mindslab.sangsangkids.iosReceiptServer.config.ConstValue;
import ai.mindslab.sangsangkids.iosReceiptServer.exception.IosReceiptException;
import ai.mindslab.sangsangkids.iosReceiptServer.model.*;
import ai.mindslab.sangsangkids.iosReceiptServer.repository.dto.DKGoodShoppingHistory;
import ai.mindslab.sangsangkids.iosReceiptServer.repository.dto.DKIosPurchaseData;
import ai.mindslab.sangsangkids.iosReceiptServer.repository.dto.DKIosPurchaseId;
import ai.mindslab.sangsangkids.iosReceiptServer.repository.mapper.IosReceiptMapper;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
public class IosReceiptService {
    private static final Logger logger = LoggerFactory.getLogger(IosReceiptService.class);
    private final Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create();

    final IosReceiptMapper mapper;

    @Value("${sangsangkids.ios.verifyServerUrl}")
    private String verifyServerUrl;

    @Value("${sangsangkids.ios.password}")
    private String appSharePassword;

    public IosReceiptService(IosReceiptMapper mapper) {
        this.mapper = mapper;
    }

    public boolean alreadyPaid(DKAlreadyPayedParams params) {
        int count = mapper.selectAlreadyPaid(params);
        logger.info(count + "");

        return count != 0;
    }

    public int verifyReceipt(IosReceiptVerifyParams params) throws IosReceiptException {
        boolean result;
        IosReceiptVerityResponse response;
        String receiptFilePath = null;

        response = getIosServerResponse(params);
        result = response.getStatus() == 0;

        if (!result) {
            logger.error((response.getStatus()) + "");
            logger.error(String.format("%s, %s", "Verify Fail", response.toString()));
            return response.getStatus();
        }

        IosNotiReceiptInfo lastPaid = getLastPaid(response);

        if (lastPaid == null) {
            return 0;
        }

        if (!params.getReceiptData().equals(response.getLatestReceipt())) {
            try {
                receiptFilePath = writeDataFile(ConstValue.RECEIPT_DIR, ConstValue.RECEIPT_SUFFIX, params.getUserId() + "", response.getLatestReceipt());
            } catch (IOException exception) {
                logger.error("Make ReceiptFile Error : " + params.getUserId());
                throw new IosReceiptException(exception);
            }
        }

        boolean isSubscribingAppleId = checkAppleSubscribing(lastPaid.getOriginalTransactionId());

        if (!isSubscribingAppleId) {
            return 204;
        }

        DKIosPurchaseId purchaseId = selectOriginalId(params, lastPaid);
        if (purchaseId == null) {
            insertOriginalId(params, lastPaid, receiptFilePath);
        }

        return 0;
    }

    private boolean checkAppleSubscribing(String originalTransactionId) {
        DKIosPurchaseId searchId = new DKIosPurchaseId();
        searchId.setOrgTranId(originalTransactionId);
        List<DKIosPurchaseId> purchaseIds = mapper.selectCheckAppleSubscribing(searchId);

        if (purchaseIds.size() == 0) return true;

        List<Integer> ishIds = new ArrayList<>();
        for (DKIosPurchaseId purchaseId : purchaseIds) {
            ishIds.add(purchaseId.getIosShoppingId());
        }

        return mapper.checkAppleSubscribing(ishIds) == 0;
    }

    private DKIosPurchaseId selectOriginalId(IosReceiptVerifyParams params, IosNotiReceiptInfo lastPaid) {
        DKIosPurchaseId iosPurchaseId = new DKIosPurchaseId();
        iosPurchaseId.setUserId(params.getUserId());
        iosPurchaseId.setOrgTranId(lastPaid.getOriginalTransactionId());
        iosPurchaseId.setActive(0);
        List<DKIosPurchaseId> iosPurchaseIds = mapper.selectIosPurchaseIdByOrgTranId(iosPurchaseId);
        if (iosPurchaseIds.size() == 0) return null;
        return iosPurchaseIds.get(0);
    }

    private String writeDataFile(String dir, String suffix, String fileName, String fileContext) throws IOException {
        Path path = Paths.get(dir);
        Files.createDirectories(path);

        String fileDir = dir + fileName + suffix;
        Path filepath = Paths.get(fileDir);
        if (!Files.exists(filepath)) {
            Files.createFile(filepath);
        }
        Files.write(filepath, fileContext.getBytes(), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);

        return fileDir;
    }

    private void insertOriginalId(IosReceiptVerifyParams params, IosNotiReceiptInfo purchaseData, String receiptFilePath) {
        DKIosPurchaseId insertData = new DKIosPurchaseId();
        int userId = params.getUserId();
        String orgTranId = purchaseData.getOriginalTransactionId();
        int result = 0;

        insertData.setUserId(userId);
        insertData.setOrgTranId(orgTranId);

        List<DKIosPurchaseId> purchaseIds = mapper.selectAlreadyRegisted(insertData);

        if (receiptFilePath != null) {
            insertData.setReceiptFilePath(receiptFilePath);
        }

        if (purchaseIds.size() > 0) {
            for (DKIosPurchaseId purchaseId : purchaseIds) {
                if (purchaseId.getUserId() != userId) {
                    if (purchaseId.getActive() == 0) {
                        purchaseId.setActive(-1);
                        mapper.updateActivationPurchaseId(purchaseId);
                    }
                }

                if (purchaseId.getActive() != 0) {
                    if (purchaseId.getUserId() == userId && purchaseId.getOrgTranId().equals(orgTranId)) {
                        purchaseId.setActive(0);
                        mapper.updateActivationPurchaseId(purchaseId);
                    }
                }
            }

        } else {
            result = mapper.insertIosShoppingId(insertData);
        }

        if (result != 1) {
            logger.error("DataBase Error");
            logger.error(insertData.toString());
        }
    }

    private IosNotiReceiptInfo getLastPaid(IosReceiptVerityResponse response) {
        List<IosNotiReceiptInfo> latestReceiptInfo = response.getLatestReceiptInfo();
        if (latestReceiptInfo.size() > 0) {
            return getLatestReceipt(latestReceiptInfo);
        }

        return null;
    }

    private IosReceiptVerityResponse getIosServerResponse(IosReceiptVerifyParams params) throws IosReceiptException {
        HttpURLConnection connection = null;

        try {
            URL url = new URL(verifyServerUrl);
            Map<String, String> verifyParam = new HashMap<>();

            verifyParam.put("receipt-data", params.getReceiptData());
            verifyParam.put("password", appSharePassword);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Accept", "application/json");
            connection.setDoOutput(true);

            OutputStream outputStream = connection.getOutputStream();
            byte[] requestData = gson.toJson(verifyParam).getBytes();
            outputStream.write(requestData);

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder responseBuilder = new StringBuilder();
            String responseLine;
            while ((responseLine = reader.readLine()) != null) {
                responseBuilder.append(responseLine.trim());
            }

            String responseJson = responseBuilder.toString();
            IosReceiptVerityResponse result;

            try {
                result = gson.fromJson(responseJson, IosReceiptVerityResponse.class);
            } catch (JsonSyntaxException e) {
                IosReceiptVerityTemp tempResponse = gson.fromJson(responseJson, IosReceiptVerityTemp.class);
                result = tempResponse.convertResponse();
            }

            return result;

        } catch (MalformedURLException exception) {
            logger.error("iOS Server URL Error");
            throw new IosReceiptException(exception);
        } catch (IOException exception) {
            logger.error("getIosServerResponse IOException");
            throw new IosReceiptException(exception);
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }

    }

    public void completePurchase(IosReceiptVerifyParams params) throws IosReceiptException {
        logger.info("Compleate_Purchase invoked");

        String receiptFilePath = null;

        IosReceiptVerityResponse response;
        response = getIosServerResponse(params);
        boolean result = response.getStatus() == 0;

        if (!result) {
            logger.error((response.getStatus()) + "");
            logger.error(String.format("%s, %s", "CompletePurchase Connection Fail", response.toString()));
            throw new IosReceiptException("CompletePurchase Connection Fail");
        }

        IosNotiReceiptInfo lastPaid = getLastPaid(response);


        if (!params.getReceiptData().equals(response.getLatestReceipt())) {
            try {
                receiptFilePath = writeDataFile(ConstValue.RECEIPT_DIR, ConstValue.RECEIPT_SUFFIX, params.getUserId() + "", response.getLatestReceipt());
            } catch (IOException exception) {
                logger.error("Make ReceiptFile Error : " + params.getUserId());
                throw new IosReceiptException(exception);
            }
        }

        DKIosPurchaseId purchaseId = selectOriginalId(params, lastPaid);
        if (purchaseId == null) {
            insertOriginalId(params, lastPaid, receiptFilePath);
        }

        insertIosShoppingHisotry(response);
    }

    public void subscribeNoti(HttpServletRequest request) throws IosReceiptException {
        StringBuilder builder = new StringBuilder();
        BufferedReader reader = null;

        try {
            InputStream inputStream = request.getInputStream();
            if (inputStream != null) {
                reader = new BufferedReader(new InputStreamReader(inputStream));
                char[] charBuffer = new char[128];
                int byteRead = -1;

                while ((byteRead = reader.read(charBuffer)) > 0) {
                    builder.append(charBuffer, 0, byteRead);
                }
            }
        } catch (IOException exception) {
            logger.error("Alert Request Reader IOException");
            throw new IosReceiptException(exception);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException exception) {
                    logger.error("Alert Request Reader Close IOException");
                    throw new IosReceiptException(exception);
                }
            }
        }

        try {
            Date date = new Date();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd_HHmmss_SSS");
            writeDataFile("request/", ".tmp", format.format(date), builder.toString());
        } catch (IOException exception) {
            exception.printStackTrace();
        }

        IosNotificationResponse response = gson.fromJson(builder.toString(), IosNotificationResponse.class);
        String notificationType = response.getNotificationType();
        logger.info(notificationType);

        switch (notificationType) {
            // 고객이 구독을 취소 했거나 업그레이드 했을 경우
            case "CANCEL":
                break;
            // 구독 갱신 상태를 변경하는 경우. 마지막 상태 업데이트 시기를 확인 할 수 있으며,
            // 현재 구독 상태에 대한 변화를 확인 할 필요 있음
            case "DID_CHANGE_RENEWAL_STATUS":
                checkChangedStatus(response);
                break;
            // 구독 청구와 관련하여 문제가 발생하여 갱신되지 않은 경우의 상태로,
            // 현재 구독에 대한 재시도 상태를 체크 후,
            // 청구 유예에 대한 기간을 확인 할 수 있음
            case "DID_FAIL_TO_RENEW":
                break;
            // 과거 갱신되지 않았던 구독이 성공적으로 갱신 된 경
            case "DID_RECOVER":
                // 고객의 구독이 거래 기간 중 성공적으로 자동 갱신 된 경우
            case "DID_RENEW":
                // 현재는 플로우 상, 구독상태 확인 프로세스랑 다를 게 없어서 동일한 플로우로 진행 예정
                checkRenew(response);
                break;
            // 최초 구매 고객의 경우
            case "INITIAL_BUY":
                // 앱 내에서 구매 한 것이 아닌 AppStore을 통해 구매 한 경
            case "INTERACTIVE_RENEWAL":
                checkChangedStatus(response);
                // 앱 구독 가격 인상에 따른 구매자 동의 시 발생하는 타입으로, 현재 지원 예정 없음.
            case "PRICE_INCREASE_CONSENT":
                // 고객이 다음 갱신 되는 구독에 대해 변경을 한 경우
                // (현재 Noti Server에서는 미처리)
            case "DID_CHANGE_RENEWAL_PREF":
                // App Store 내에서 정상적으로 환불이 된 경우로, 정상적인 케이스가 아니어서 별도 처리 없음.
            case "REFUND":
            default:
                insertOtherCaseNotification(response);
        }

    }

    private IosNotiReceiptInfo getLastPurchaseInfo(IosNotificationResponse response) throws IosReceiptException {
        List<IosNotiReceiptInfo> latestReceipts = response.getUnifiedReceipt().getLatestReceiptInfo();
        if (latestReceipts.size() == 0) {
            logger.error("Not Include Receipts");
            Date date = new Date();
            SimpleDateFormat dirFormat = new SimpleDateFormat("yyyy/MM/dd/HH");
            SimpleDateFormat fileFormat = new SimpleDateFormat("MM-ss");
            String exceptionDir = ConstValue.EXCEPTION_DIR + dirFormat.format(date);
            try {
                writeDataFile(exceptionDir, ConstValue.EXCEPTION_SUFFIX, fileFormat.format(date), response.toString());
            } catch (IOException exception) {
                throw new IosReceiptException("File Write Error");
            }
            return null;
        }
        IosNotiReceiptInfo lastOne = latestReceipts.get(latestReceipts.size() - 1);
        logger.info(lastOne.toString());
        String orgTranId = lastOne.getOriginalTransactionId();
        List<DKIosPurchaseId> purchaseIds = mapper.selectIosPurchaseIdByOrgTranId(new DKIosPurchaseId(orgTranId));
        if (purchaseIds.size() > 1) {
            logger.warn(String.format("Over Size orgTranId (%d) : %s", purchaseIds.size(), orgTranId));
        }

        if (purchaseIds.size() != 0) {
            lastOne.setUserId(purchaseIds.get(0).getUserId());
        }

        return lastOne;
    }

    private void insertOtherCaseNotification(IosNotificationResponse response) throws IosReceiptException {
        Date date = new Date();
        SimpleDateFormat dirFormat = new SimpleDateFormat("/yyyy-MM/dd");
        SimpleDateFormat fileFormat = new SimpleDateFormat("HH-mm-ss-SSS");

        try {
            writeDataFile(ConstValue.OTHERCASE_DIR + dirFormat.format(date), ConstValue.OTHERCASE_SUFFIX, fileFormat.format(date), response.toString());
        } catch (IOException exception) {
            logger.error("OtherCase Write File Exception : " + exception.getMessage(), exception);
            throw new IosReceiptException("OtherCase Write File Exception");
        }
    }

    private void insertPurchaseData(IosNotiReceiptInfo purchaseData) throws IosReceiptException {

        if (purchaseData == null) {
            return;
        }

        DKGoodShoppingHistory historyData = parseHistoryData(purchaseData);

        if (mapper.countDkShoppingHistroyByWebOrderId(historyData.getWebOrderId()) > 0) {
            logger.warn("Already Inserted WebOrderId : " + historyData.getWebOrderId());
            return;
        }

        if (mapper.insertDKShoppingHistory(historyData) != 1) {
            throw new IosReceiptException("Data Insert Error");
        }

    }

    private DKGoodShoppingHistory parseHistoryData(IosNotiReceiptInfo purchaseData) {
        String startDate = parseMsToKSTDate(purchaseData.getPurchaseDateMs());
        String endDate = parseMsToKSTDate(purchaseData.getExpiresDateMs());
        int voucherNo = parseVoucherNo(purchaseData.getProductId());
        return new DKGoodShoppingHistory(purchaseData.getUserId(), voucherNo, startDate, endDate, purchaseData.getWebOrderLineItemId());
    }

    private int parseVoucherNo(String productId) {
        int result;
        switch (productId) {
            case "bookjam_sori_1month" : result = 2; break;
            case "bookjam_total_1month_ios" :
            default: result = 1;
        }

        return result;
    }

    private String parseMsToKSTDate(String msData) {
        long ms = Long.parseLong(msData);
        Instant instant = Instant.ofEpochMilli(ms);
        ZonedDateTime kstTime = instant.atZone(ZoneId.of("Asia/Seoul"));
        return kstTime.format(DateTimeFormatter.ofPattern("yyyyMMdd"));
    }

    private boolean checkChangedStatus(IosNotificationResponse response) throws IosReceiptException {
        IosNotiReceiptInfo purchaseData = getLastPurchaseInfo(response);
        String orderId;

        try {
            orderId = purchaseData.getWebOrderLineItemId();
        } catch (NullPointerException exception) {
            throw new IosReceiptException("WebOrderID NPE");
        }
        if (mapper.countIosShoppingHistoryByOrderId(orderId) > 0) {
            logger.info("already Inserted Data : " + orderId);
            return false;
        }
        insertIosShoppingHisotry(response);
        return true;
    }

    private void insertIosShoppingHisotry(IosNotificationResponse response) {
        IosNotiReceiptInfo lastReceipt = getLatestReceipt(response.getUnifiedReceipt().getLatestReceiptInfo());

        DKIosPurchaseId purchaseId = new DKIosPurchaseId(lastReceipt.getOriginalTransactionId());
        purchaseId = mapper.selectIosPurchaseIdByOrgTranId(purchaseId).get(0);
        lastReceipt.setUserId(purchaseId.getUserId());

        insertIosShoppingHisotry(purchaseId, lastReceipt.getExpiresDateMs(), lastReceipt.getProductId(), response.getAutoRenewStatus(), response.getNotificationType(), lastReceipt.getWebOrderLineItemId());

        try {
            insertPurchaseData(lastReceipt);
        } catch (IosReceiptException exception) {
            exception.printStackTrace();
        }
    }

    private void insertIosShoppingHisotry(IosReceiptVerityResponse verityResponse) {
        IosNotiReceiptInfo lastReceipt = getLatestReceipt(verityResponse.getLatestReceiptInfo());

        DKIosPurchaseId purchaseId = new DKIosPurchaseId(lastReceipt.getOriginalTransactionId());
        purchaseId = mapper.selectIosPurchaseIdByOrgTranId(purchaseId).get(0);
        lastReceipt.setUserId(purchaseId.getUserId());
        lastReceipt.setIshId(purchaseId.getIosShoppingId());

        insertIosShoppingHisotry(purchaseId, lastReceipt.getExpiresDateMs(), lastReceipt.getProductId(), "OtherCase", "APPLICATION_CALL", lastReceipt.getWebOrderLineItemId());

        try {
            insertPurchaseData(lastReceipt);
        } catch (IosReceiptException exception) {
            exception.printStackTrace();
        }
    }

    private IosNotiReceiptInfo getLatestReceipt(List<IosNotiReceiptInfo> receiptInfoList) {
        long latestPurchaseDateMs = 0L;
        IosNotiReceiptInfo latestReceipt = null;

        for (IosNotiReceiptInfo receiptInfo : receiptInfoList) {
            long thisPurchaseDateMs = Long.parseLong(receiptInfo.getPurchaseDateMs());
            if (latestPurchaseDateMs < thisPurchaseDateMs) {
                latestPurchaseDateMs = thisPurchaseDateMs;
                latestReceipt = receiptInfo;
            }
        }

        return latestReceipt;
    }

    private void insertIosShoppingHisotry (DKIosPurchaseId purchaseId, String expDateMs, String productId, String renewStatus, String notificationType, String webOrderId) {
        String expDate = parseMsToKSTDate(expDateMs);

        DKIosPurchaseData historyData = new DKIosPurchaseData(purchaseId.getIosShoppingId(), expDate, productId, renewStatus, notificationType, webOrderId);

        if (mapper.selectIosShoppingHisByWebOrderId(webOrderId) == 0) {
            mapper.insertDKIosShoppingHis(historyData);
        }
    }

    private void checkRenew(IosNotificationResponse response) throws IosReceiptException {
        checkChangedStatus(response);
    }
}
