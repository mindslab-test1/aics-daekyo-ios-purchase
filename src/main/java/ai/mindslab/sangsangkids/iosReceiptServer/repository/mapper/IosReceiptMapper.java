package ai.mindslab.sangsangkids.iosReceiptServer.repository.mapper;

import ai.mindslab.sangsangkids.iosReceiptServer.model.DKAlreadyPayedParams;
import ai.mindslab.sangsangkids.iosReceiptServer.repository.dto.DKGoodShoppingHistory;
import ai.mindslab.sangsangkids.iosReceiptServer.repository.dto.DKIosPurchaseData;
import ai.mindslab.sangsangkids.iosReceiptServer.repository.dto.DKIosPurchaseId;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IosReceiptMapper {
    int selectAlreadyPaid(DKAlreadyPayedParams params);
    int insertIosShoppingId(DKIosPurchaseId insertData);
    List<DKIosPurchaseId> selectAlreadyRegisted(DKIosPurchaseId purchaseId);
    List<DKIosPurchaseId> selectIosPurchaseIdByOrgTranId(DKIosPurchaseId iosPurchaseId);
    int countIosShoppingHistoryByOrderId(String orderId);
    int insertDKIosShoppingHis(DKIosPurchaseData iosHistoryData);
    int insertDKShoppingHistory(DKGoodShoppingHistory historyData);
    int checkAppleSubscribing(List<Integer> ishIds);
    int selectIosShoppingHisByWebOrderId(String webOrderId);
    int countDkShoppingHistroyByWebOrderId(String webOrderId);
    int updateActivationPurchaseId(DKIosPurchaseId purchaseId);

    List<DKIosPurchaseId> selectCheckAppleSubscribing(DKIosPurchaseId searchId);
}
