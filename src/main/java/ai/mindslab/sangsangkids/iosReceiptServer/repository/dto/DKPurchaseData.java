package ai.mindslab.sangsangkids.iosReceiptServer.repository.dto;

import lombok.Data;

@Data
public class DKPurchaseData {
    private int userId;
    private int seqNo;
    private int voucherSeqNo;
    private String startDate;
    private String endDate;
    private String sortation;
}
