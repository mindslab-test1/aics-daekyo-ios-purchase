package ai.mindslab.sangsangkids.iosReceiptServer.repository.dto;

import lombok.Data;

import java.util.Date;

@Data
public class DKIosPurchaseId {
    private int iosShoppingId;
    private int userId;
    private String orgTranId;
    private String receiptFilePath;
    private Date crtDate;
    private int active;

    public DKIosPurchaseId() {}

    public DKIosPurchaseId(String orgTranId) {
        this.orgTranId = orgTranId;
        this.active = 0;
    }
}
