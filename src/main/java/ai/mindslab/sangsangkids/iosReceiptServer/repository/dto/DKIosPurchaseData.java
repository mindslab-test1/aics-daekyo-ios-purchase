package ai.mindslab.sangsangkids.iosReceiptServer.repository.dto;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class DKIosPurchaseData {
    private int ishId;
    private int iosShoppingId;
    private String expDate;
    private String goodId;
    private String renewState;
    private String notiType;
    private String webOrderId;

    public DKIosPurchaseData(int iosShoppingId, String expDate, String goodId, String renewState, String notiType, String webOrderId) {
        this.iosShoppingId = iosShoppingId;
        this.expDate = expDate;
        this.goodId = goodId;
        this.renewState = renewState;
        this.notiType = notiType;
        this.webOrderId = webOrderId;
    }
}
