package ai.mindslab.sangsangkids.iosReceiptServer.repository.dto;

import lombok.Data;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
public class DKGoodShoppingHistory {
    private int userId;
    private int seqNo;
    private int voucherSeqNo;
    private String startDate;
    private String endDate;
    private Date crtDate;
    private Date updDate;
    private String sortation;
    private int promotionId;
    private String token;
    private String notificationtypeCode;
    private String regDate;
    private String buyYn;
    private String platform;
    private String webOrderId;

    public DKGoodShoppingHistory(int userId, int voucherSeqNo, String startDate, String endDate, String webOrderId) {
        this.voucherSeqNo = voucherSeqNo;
        this.promotionId = 1;
        this.sortation = "BUY";
        this.buyYn = "Y";
        this.platform = "iOS";

        this.userId = userId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.webOrderId = webOrderId;
    }
}
