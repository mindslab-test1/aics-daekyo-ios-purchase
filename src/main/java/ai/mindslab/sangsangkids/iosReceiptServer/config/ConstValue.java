package ai.mindslab.sangsangkids.iosReceiptServer.config;

public class ConstValue {
    private static final String BASE_DIR = "tmp/";
    public static final String RECEIPT_DIR = BASE_DIR + "receipt/";
    public static final String RECEIPT_SUFFIX = ".rece";

    public static final String EXCEPTION_DIR = BASE_DIR + "exception/";
    public static final String EXCEPTION_SUFFIX = ".exce";

    public static final String OTHERCASE_DIR = BASE_DIR + "other_case/";
    public static final String OTHERCASE_SUFFIX = ".case";
}
