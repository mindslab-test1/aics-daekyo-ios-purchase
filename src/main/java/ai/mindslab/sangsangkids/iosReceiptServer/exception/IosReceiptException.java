package ai.mindslab.sangsangkids.iosReceiptServer.exception;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class IosReceiptException extends Exception {
    private HttpStatus state;
    private String message;

    public IosReceiptException(String message) {
        this(HttpStatus.INTERNAL_SERVER_ERROR, message);
    }

    public IosReceiptException(HttpStatus state, String message) {
        this.state = state;
        this.message = message;
    }

    public IosReceiptException(Exception e) {
        this.state = HttpStatus.INTERNAL_SERVER_ERROR;
        this.message = e.getMessage();
    }
}
